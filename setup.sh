## This script configres the postfix with dovecot and sasl auth
## This script is written by Ahmad Hesamzadeh
## Original source: http://library.linode.com/email/postfix/dovecot-mysql-debian-6-squeeze
## 
## NOTE: I've tested the instruction in the above link and works well,
## But I have not tested this script yet and I'm not sure about its quality

#Mysql root user's password
MYSQL_ROOT_PASS=password

#Users and Password for database user for mail
MAIL_ADMIN_USER=mail_admin
MAIL_ADMIN_PASS=password
MAIL_DB_NAME=mail

#Hostname
MY_HOSTNAME=aegir.local


#Install required packages
apt-get update
apt-get install -y postfix postfix-mysql postfix-doc mysql-client mysql-server dovecot-common dovecot-imapd dovecot-pop3d libsasl2-2 libsasl2-modules libsasl2-modules-sql sasl2-bin libpam-mysql openssl telnet mailutils

#Create mysql database
mysql -u root -p$MYSQL_ROOT_PASS -e <<EOF
CREATE DATABASE $MAIL_DB_NAME;
USE $MAIL_DB_NAME;

GRANT SELECT, INSERT, UPDATE, DELETE ON $MAIL_DB_NAME.* TO '$MAIL_ADMIN_USER'@'localhost' IDENTIFIED BY '$MAIL_ADMIN_PASS';
GRANT SELECT, INSERT, UPDATE, DELETE ON $MAIL_DB_NAME.* TO '$MAIL_ADMIN_USER'@'localhost.localdomain' IDENTIFIED BY '$MAIL_ADMIN_PASS';
FLUSH PRIVILEGES;

CREATE TABLE domains (domain varchar(50) NOT NULL, PRIMARY KEY (domain) );
CREATE TABLE forwardings (source varchar(80) NOT NULL, destination TEXT NOT NULL, PRIMARY KEY (source) );
CREATE TABLE users (email varchar(80) NOT NULL, password varchar(20) NOT NULL, PRIMARY KEY (email) );
CREATE TABLE transport ( domain varchar(128) NOT NULL default '', transport varchar(128) NOT NULL default '', UNIQUE KEY domain (domain) );
EOF

mysql_secure_installation
service mysql restart

#Configure Postfix to work with MySQL
#Create query files
cat > /etc/postfix/mysql-virtual_domains.cf <<EOL
user = $MAIL_ADMIN_USER
password = $MAIL_ADMIN_PASS
dbname = $MAIL_DB_NAME
query = SELECT domain AS virtual FROM domains WHERE domain='%s'
hosts = 127.0.0.1
EOL

cat > /etc/postfix/mysql-virtual_forwardings.cf <<EOL
user = $MAIL_ADMIN_USER
password = $MAIL_ADMIN_PASS
dbname = $MAIL_DB_NAME
query = SELECT destination FROM forwardings WHERE source='%s'
hosts = 127.0.0.1
EOL

cat > /etc/postfix/mysql-virtual_mailboxes.cf <<EOL
user = $MAIL_ADMIN_USER
password = $MAIL_ADMIN_PASS
dbname = $MAIL_DB_NAME
query = SELECT CONCAT(SUBSTRING_INDEX(email,'@',-1),'/',SUBSTRING_INDEX(email,'@',1),'/') FROM users WHERE email='%s'
hosts = 127.0.0.1
EOL

cat > /etc/postfix/mysql-virtual_email2email.cf <<EOL
user = $MAIL_ADMIN_USER
password = $MAIL_ADMIN_PASS
dbname = $MAIL_DB_NAME
query = SELECT email FROM users WHERE email='%s'
hosts = 127.0.0.1
EOL

#Set proper permissions and ownership for these configuration files by issuing the following commands:
chmod o= /etc/postfix/mysql-virtual_*.cf
chgrp postfix /etc/postfix/mysql-virtual_*.cf

#Next, create a user and group for mail handling. All virtual mailboxes will be stored under this user's home directory.
groupadd -g 5000 vmail
useradd -g vmail -u 5000 vmail -d /home/vmail -m

#Configure postfix
postconf -e "myhostname = $MY_HOSTNAME"
postconf -e "mydestination = $MY_HOSTNAME, localhost, localhost.localdomain"
postconf -e 'mynetworks = 127.0.0.0/8'
postconf -e 'message_size_limit = 30720000'
postconf -e 'virtual_alias_domains ='
postconf -e 'virtual_alias_maps = proxy:mysql:/etc/postfix/mysql-virtual_forwardings.cf, mysql:/etc/postfix/mysql-virtual_email2email.cf'
postconf -e 'virtual_mailbox_domains = proxy:mysql:/etc/postfix/mysql-virtual_domains.cf'
postconf -e 'virtual_mailbox_maps = proxy:mysql:/etc/postfix/mysql-virtual_mailboxes.cf'
postconf -e 'virtual_mailbox_base = /home/vmail'
postconf -e 'virtual_uid_maps = static:5000'
postconf -e 'virtual_gid_maps = static:5000'
postconf -e 'smtpd_sasl_auth_enable = yes'
postconf -e 'broken_sasl_auth_clients = yes'
postconf -e 'smtpd_sasl_authenticated_header = yes'
postconf -e 'smtpd_recipient_restrictions = permit_mynetworks, permit_sasl_authenticated, reject_unauth_destination'
postconf -e 'smtpd_use_tls = yes'
postconf -e 'smtpd_tls_cert_file = /etc/postfix/smtpd.cert'
postconf -e 'smtpd_tls_key_file = /etc/postfix/smtpd.key'
postconf -e 'virtual_create_maildirsize = yes'
postconf -e 'virtual_maildir_extended = yes'
postconf -e 'proxy_read_maps = $local_recipient_maps $mydestination $virtual_alias_maps $virtual_alias_domains $virtual_mailbox_maps $virtual_mailbox_domains $relay_recipient_maps $relay_domains $canonical_maps $sender_canonical_maps $recipient_canonical_maps $relocated_maps $transport_maps $mynetworks $virtual_mailbox_limit_maps'
postconf -e virtual_transport=dovecot
postconf -e dovecot_destination_recipient_limit=1

#Create an SSL Certificate for Postfix
cd /etc/postfix
openssl req -new -outform PEM -out smtpd.cert -newkey rsa:2048 -nodes -keyout smtpd.key -keyform PEM -days 365 -x509
chmod o= /etc/postfix/smtpd.key

#Configure saslauthd to use MySQL
mkdir -p /var/spool/postfix/var/run/saslauthd
cp -a /etc/default/saslauthd /etc/default/saslauthd.bak

cat > /etc/default/saslauthd <<EOL
START=yes
DESC="SASL Authentication Daemon"
NAME="saslauthd"
MECHANISMS="pam"
MECH_OPTIONS=""
THREADS=5
OPTIONS="-c -m /var/spool/postfix/var/run/saslauthd -r"
EOL

cat > /etc/pam.d/smtp <<EOL
auth    required   pam_mysql.so user=$MAIL_ADMIN_USER passwd=$MAIL_ADMIN_PASS host=127.0.0.1 db=$MAIL_DB_NAME table=users usercolumn=email passwdcolumn=password crypt=1
account sufficient pam_mysql.so user=$MAIL_ADMIN_USER passwd=$MAIL_ADMIN_PASS host=127.0.0.1 db=$MAIL_DB_NAME table=users usercolumn=email passwdcolumn=password crypt=1
EOL

cat > /etc/postfix/sasl/smtpd.conf <<EOL
pwcheck_method: saslauthd
mech_list: plain login
allow_plaintext: true
auxprop_plugin: mysql
sql_hostnames: 127.0.0.1
sql_user: $MAIL_ADMIN_USER
sql_passwd: $MAIL_ADMIN_PASS
sql_database: $MAIL_DB_NAME
sql_select: select password from users where email = '%u'
EOL

#Set proper permissions and ownership for these configuration files by issuing the following commands:
chmod o= /etc/pam.d/smtp
chmod o= /etc/postfix/sasl/smtpd.conf

#Add the Postfix user to the sasl group and restart Postfix and saslauthd by issuing the following commands:
adduser postfix sasl
service postfix restart
service saslauthd restart

#Configure Dovecot

cat >> /etc/postfix/master.cf <<EOL
dovecot   unix  -       n       n       -       -       pipe
    flags=DRhu user=vmail:vmail argv=/usr/lib/dovecot/deliver -d ${recipient}
EOL

cp -a /etc/dovecot/dovecot.conf /etc/dovecot/dovecot.conf.bak
cat > /etc/dovecot/dovecot.conf <<EOL
protocols = imap imaps pop3 pop3s
log_timestamp = "%Y-%m-%d %H:%M:%S "
mail_location = maildir:/home/vmail/%d/%n/Maildir

ssl_cert_file = /etc/ssl/certs/dovecot.pem
ssl_key_file = /etc/ssl/private/dovecot.pem

namespace private {
    separator = .
    prefix = INBOX.
    inbox = yes
}

protocol lda {
    log_path = /home/vmail/dovecot-deliver.log
    auth_socket_path = /var/run/dovecot/auth-master
    postmaster_address = postmaster@$MY_HOSTNAME
    mail_plugins = sieve
    global_script_path = /home/vmail/globalsieverc
}

protocol pop3 {
    pop3_uidl_format = %08Xu%08Xv
}

auth default {
    user = root

    passdb sql {
        args = /etc/dovecot/dovecot-sql.conf
    }

    userdb static {
        args = uid=5000 gid=5000 home=/home/vmail/%d/%n allow_all_users=yes
    }

    socket listen {
        master {
            path = /var/run/dovecot/auth-master
            mode = 0600
            user = vmail
        }

        client {
            path = /var/spool/postfix/private/auth
            mode = 0660
            user = postfix
            group = postfix
        }
    }
}
EOL


cp -a /etc/dovecot/dovecot-sql.conf /etc/dovecot/dovecot-sql.conf.bak
cat > /etc/dovecot/dovecot-sql.conf <<EOL
driver = mysql
connect = host=127.0.0.1 dbname=$MAIL_DB_NAME user=$MAIL_ADMIN_USER password=$MAIL_ADMIN_PASS
default_pass_scheme = CRYPT
password_query = SELECT email as user, password FROM users WHERE email='%u';
EOL

service dovecot restart

chgrp vmail /etc/dovecot/dovecot.conf
chmod g+r /etc/dovecot/dovecot.conf

#Configure Mail Aliases
echo "root: postmaster@$MY_HOSTNAME" >> /etc/aliases

newaliases
service postfix restart

#finish
